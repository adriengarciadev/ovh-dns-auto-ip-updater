import os
from pathlib import Path

import requests

from dotenv import load_dotenv
import json
import ovh


def main():
    load_dotenv()
    check_required_env_variables()

    if not is_current_ip_new():
        print('Current IP is not new, exiting...')
        exit(0)

    ovh_domain_zone_export = get_domain_zone_export()
    ovh_domain_zone_to_import = ovh_domain_zone_export.replace(old_target_ip, current_ip)
    import_domain_zone(ovh_domain_zone_to_import)
    update_old_target_ip(current_ip)


def update_old_target_ip(ip_address: str):
    target_ip_file_content = {
        'old_target_ip': ip_address
    }
    with open('target_ip.json', 'w') as target_ip_file:
        json.dump(target_ip_file_content, target_ip_file)


def import_domain_zone(text_to_import: str):
    text_length = len(text_to_import)
    if text_length < int(os.getenv('MIN_ZONE_TEXT_LENGTH')):
        print(f'Generated zone text file is {text_length} chars '
              f'which is lower than MIN_ZONE_TEXT_LENGTH, exiting...')
        exit(1)
    ovh_client = get_ovh_client()
    domain = os.getenv('OVH_DOMAIN')
    return ovh_client.post(
        f'/domain/zone/{domain}/import',
        zoneFile=text_to_import,
    )


def get_domain_zone_export():
    ovh_client = get_ovh_client()
    domain = os.getenv('OVH_DOMAIN')
    return ovh_client.get(f'/domain/zone/{domain}/export')


def is_current_ip_new():
    global old_target_ip, current_ip
    current_ip = requests.get('https://api.myip.com').json()['ip']
    if Path('target_ip.json').is_file():
        old_target_ip = get_old_target_ip()
    else:
        handle_missing_target_ip_file(current_ip)

    return old_target_ip != current_ip


def handle_missing_target_ip_file(ip: str):
    target_ip_file_content = {
        'old_target_ip': ip
    }
    with open('target_ip.json', 'w') as target_ip_file:
        json.dump(target_ip_file_content, target_ip_file)
    print('target_ip.json not found, generating one and exiting...')
    exit(0)


def get_old_target_ip():
    target_ip_file = open('target_ip.json', 'r')
    return json.load(target_ip_file)['old_target_ip']


def check_required_env_variables():
    required_env_variables = [
        'OVH_DOMAIN',
        'OVH_ENDPOINT',
        'APPLICATION_KEY',
        'APPLICATION_SECRET',
        'CONSUMER_KEY'
    ]
    for variable in required_env_variables:
        check_variable(variable)


def check_variable(variable_string: str):
    if not os.getenv(variable_string):
        print(f'{variable_string} is required, please set it in .env and retry')
        exit(1)


def get_ovh_client():
    return ovh.Client(
        endpoint=os.getenv('OVH_ENDPOINT'),
        application_key=os.getenv('APPLICATION_KEY'),
        application_secret=os.getenv('APPLICATION_SECRET'),
        consumer_key=os.getenv('CONSUMER_KEY'),
    )


if __name__ == '__main__':
    old_target_ip = ''
    current_ip = ''
    main()
