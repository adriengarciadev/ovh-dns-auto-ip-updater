# ovh dns auto ip updater

This tool is used to update automatically your OVH dns domain (only one for now) records with current IP address

## Prerequisites 

- python3.9

Python packages: 

- dotenv
- ovh

## How to use ?

1. First, your ovh conf should be working before using this script.
2. Make a backup of your domain zone file to prevent any data loss
3. [Create API keys](https://eu.api.ovh.com/createToken) allowed to access to:
   - `GET: /domain/zone/{domain}/export`
   - `POST: /domain/zone/{domain}/import`
4. Download the content of `src` into a fresh folder
5. Position yourself in that folder
```bash
cd freshFolder
```
6. Copy `.env.sample` to `.env`
```bash
cp .env.sample .env
```
7. Fill in variables in `.env` with your information using your favorite editor
8. Launch script using command:
````bash
python3 main.py
````
